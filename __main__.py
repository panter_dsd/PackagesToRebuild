import subprocess
import datetime
import re


class InstallHistoryEntry(object):
    def __init__(self, name: str, install_date_time: datetime.datetime):
        super().__init__()
        self._name = name
        self._install_date_time = datetime.datetime.fromtimestamp(install_date_time)
        self._real_name = None

    def compare(self, name: str) -> bool:
        return self._name.startswith(name + '-') and self._name[len(name) + 1].isnumeric()

    def name(self) -> str:
        return self._name

    def install_date_time(self) -> datetime.datetime:
        return self._install_date_time

    def real_name(self) -> str:
        return self._real_name

    def set_real_name(self, name: str):
        self._real_name = name


def installed_package_names() -> list:
    result = subprocess.check_output(["eix", "-I", "--only-names"])
    return result.decode("utf-8").split("\n")


def install_history() -> list:
    line_re = re.compile("(\d+).*\) (\S+).*")

    result = []
    with open("/var/log/emerge.log", 'r') as file:
        for line in file.readlines():
            if "completed emerge" in line:
                m = line_re.match(line)
                if m:
                    entry = InstallHistoryEntry(m.groups()[1], int(m.groups()[0]))
                    result.append(entry)

    return result

def last_installed(package_name: str, history: list) -> InstallHistoryEntry:
    result = None

    for entry in history:
        if entry.compare(package_name):
            if not result or result.install_date_time() < entry.install_date_time():
                result = entry

    return result

def installed_packages() -> list:
    result = []

    install_history_list = install_history()
    for package in installed_package_names():
        entry = last_installed(package, install_history_list)
        if entry:
            entry.set_real_name(package)
            result.append(entry)

    return result

current_date_time = datetime.datetime.now() - datetime.timedelta(20)

for entry in installed_packages():
    if entry.install_date_time() < current_date_time:
        #print(entry.real_name() + " -> " + entry.install_date_time().strftime("%Y-%m-%d %H:%M:%S"))
        print(entry.real_name())
